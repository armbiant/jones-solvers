""" Unit tests for Jones matrix solvers


"""
import logging
import sys
import unittest

import astropy.constants as consts
import astropy.units as u
import numpy as np
from astropy.coordinates import SkyCoord
from astropy.time import Time
from numpy import cos as cos
from numpy import sin as sin
from rascil.data_models import PolarisationFrame
from rascil.processing_components import (
    create_blockvisibility,
    create_named_configuration,
)
from rascil.processing_components.calibration.operations import (
    create_gaintable_from_blockvisibility,
)
from rascil.processing_components.util.coordinate_support import (
    lmn_to_skycoord,
)
from rascil.processing_components.util.geometry import calculate_azel

from jones_solvers.processing_components import solve_jones

#log = logging.getLogger("jones-logger")
log = logging.getLogger()

log.setLevel(logging.WARNING)
#log.setLevel(logging.INFO)
#log.setLevel(logging.DEBUG)
log.addHandler(logging.StreamHandler(sys.stdout))

class TestCalibrationSolvers(unittest.TestCase):

    def setUp(self):
        np.random.seed(180555)

    def visSetup(
        self,
        vnchan=3,
        ntimes=3,
        rmax=250.0
    ):

        lowcore = create_named_configuration("LOWBD2", rmax=rmax)
        lon = 116.76444824 * np.pi / 180.
        lat = -26.82472208 * np.pi / 180.

        integration_time = 1.0
        times = (np.pi / 43200.0) * np.arange(0,ntimes*integration_time, integration_time)

        channel_bandwidth = 1.0e6
        frequency = np.arange(100.0e6, 100.0e6+vnchan*channel_bandwidth, channel_bandwidth)
        channel_bandwidth = np.array(vnchan*[channel_bandwidth])

        phasecentre = SkyCoord(ra=+15.0 * u.deg, dec=-45.0 * u.deg, frame='icrs', equinox='J2000')

        # create empty blockvis with intrumental polarisation (XX, XY, YX, YY)
        self.modelvis = create_blockvisibility(lowcore, times, frequency, channel_bandwidth=channel_bandwidth,
                                      phasecentre=phasecentre, integration_time=integration_time,
                                      polarisation_frame=PolarisationFrame("linear"), weight=1.0)

        self.assertEqual( self.modelvis['vis'].shape[0], ntimes )
        self.assertEqual( self.modelvis['vis'].shape[2], vnchan )
        self.assertEqual( self.modelvis['vis'].shape[3], 4 )
        self.assertEqual( self.modelvis['vis'].shape[0:3], self.modelvis["uvw_lambda"].data.shape[0:3] )
        self.assertTrue( all(self.modelvis['polarisation'].data == ['XX', 'XY', 'YX', 'YY']) )

        nvis = self.modelvis["baselines"].shape[0]

        # ------------------------- #

        log.info("Gen sky model and updating vis")

        Nsrc = 10

        dist_source_max = 2.5 * np.pi/180.0

        # sky model: randomise sources across the field
        theta = 2.*np.pi * np.random.rand(Nsrc) 
        phi = dist_source_max * np.sqrt( np.random.rand(Nsrc) )
        l = sin(theta) * sin(phi)
        m = cos(theta) * sin(phi)
        n = np.sqrt(1-l*l-m*m) - 1
        jy = 10 * np.random.rand(Nsrc)
        for src in range(0,Nsrc):

            # analytic response of short dipoles aligned NS & EW to sky xy polarisations
            # with an approx Gaussian taper for a 35m station
            srcdir = lmn_to_skycoord(np.array([l[src],m[src],n[src]]), phasecentre)
            ra  = srcdir.ra.value * np.pi / 180.
            dec = srcdir.dec.value * np.pi / 180.
            sep = srcdir.separation(phasecentre).value * np.pi / 180.
            diam = 35.;

            # need to set ha,dec, but need to be in time,freq loop
            for t in range(0,len(self.modelvis['datetime'])):

                utc_time = self.modelvis['datetime'].data[t]
                #azel = calculate_azel(location, utc_time, srcdir);            
                lst = Time(utc_time, location=(lon * u.rad, lat * u.rad)).sidereal_time('mean').value * np.pi / 12.
                ha = lst - ra

                J00 =  cos(lat)*cos(dec) + sin(lat)*sin(dec)*cos(ha)
                J01 = -sin(lat)*sin(ha)
                J10 =  sin(dec)*sin(ha)
                J11 =  cos(ha)
                J = np.array([[J00,J01],[J10,J11]], "complex")
                # components are unpolarised, so can form power product now
                JJ = J @ J.conj().T

                for f in range(0,len(self.modelvis['frequency'])):

                    wl = consts.c.value / self.modelvis['frequency'].data[f];
                    sigma = wl/diam / 2.355;
                    gain = np.exp( -sep*sep/(2*sigma*sigma) );

                    srcbeam = JJ * gain

                    # vis (time, baselines, frequency, polarisation) complex128

                    uvw = self.modelvis['uvw_lambda'].data[t,:,f,:]
                    phaser = 0.5*jy[src] * np.exp( 2j*np.pi * (uvw[:,0]*l[src] + uvw[:,1]*m[src] + uvw[:,2]*n[src]) )

                    self.modelvis['vis'].data[t,:,f,0] += phaser * srcbeam[0,0]
                    self.modelvis['vis'].data[t,:,f,1] += phaser * srcbeam[0,1]
                    self.modelvis['vis'].data[t,:,f,2] += phaser * srcbeam[1,0]
                    self.modelvis['vis'].data[t,:,f,3] += phaser * srcbeam[1,1]

        # ------------------------- #

        log.info("Applying calibration factors and noise")

        # Make copies of the vis and apply calibration factors and noise
        self.vis = self.modelvis.copy(deep=True)

        stations = lowcore["stations"]
        nstations = stations.shape[0]

        stn1 = self.modelvis["antenna1"].data
        stn2 = self.modelvis["antenna2"].data

        # set up station Jones matrices (gains and leakages)
        Jsigma = 0.1
        self.modeljones  = create_gaintable_from_blockvisibility(self.modelvis, timeslice=1e6, jones_type="G")
        self.truejones = create_gaintable_from_blockvisibility(self.modelvis, timeslice=1e6, jones_type="G")

        # set up references to the data
        Jm = self.modeljones["gain"].data
        Jt = self.truejones["gain"].data

        for stn in range(0,nstations):
            # generate the starting model station gain error matrices. The same for all tests
            Jm[0,stn,0,:,:] = np.eye(2, dtype=complex)
            # generate the true station gain error matrices. Set to model matrices plus some Gaussian offsets
            Jt[0,stn,0,:,:] = Jm[0,stn,0,:,:] + Jsigma * ( np.random.randn(2,2) + 1j*np.random.randn(2,2) )

        # Loop over visibilities and multiply in the Jones matrices
        for t in range(0,len(self.modelvis['datetime'])):
            for f in range(0,len(self.modelvis['frequency'])):
                # set up references to the data
                modeltmp = self.modelvis['vis'].data[t,:,f,:]
                vistmp   = self.vis['vis'].data[t,:,f,:]
                for k in range(0,nvis):

                    vis_in = np.reshape(modeltmp[k,:],(2,2))
                    vis_out = Jm[0,stn1[k],0] @ vis_in @ Jm[0,stn2[k],0].conj().T
                    modeltmp[k,:] = np.reshape(np.array(vis_out),(4))

                    vis_in = np.reshape(vistmp[k,:],(2,2))
                    vis_out = Jt[0,stn1[k],0] @ vis_in @ Jt[0,stn2[k],0].conj().T
                    vistmp[k,:] = np.reshape(np.array(vis_out),(4))

    def test_solve(self):
        self.visSetup(vnchan=1, ntimes=1, rmax=400.0)

        # log.info("Adding noise to visibilities")
        # sigma = 0.0
        # shape = self.vis['vis'].shape
        # assert len(shape) == 4, "require 4 dimensions for blockvisibilty"
        # self.vis['vis'].data += sigma * np.random.randn(shape[0],shape[1],shape[2],shape[3])
        # if sigma > 0: self.vis['weight'].data = 1. / (sigma * sigma)

        log.info("Solving calibration")
        solve_jones(self.vis, self.modelvis, self.modeljones, testvis=self.vis)

        log.info("vis max:  {}".format(np.max(np.abs(self.vis['vis'].data))))
        log.info("diff max: {}".format(np.max(np.abs(self.modelvis['vis'].data-self.vis['vis'].data))))
        log.info("vis stdev:  {}".format(np.std(self.vis['vis'].data)))
        log.info("diff stdev: {}".format(np.std(self.modelvis['vis'].data-self.vis['vis'].data)))
        self.assertTrue( np.max(np.abs(self.modelvis['vis'].data-self.vis['vis'].data)) < 1e-2 )
        self.assertTrue( np.std(self.modelvis['vis'].data-self.vis['vis'].data) < 1e-2 )

    def test_accum_opt(self):
        self.visSetup()

        modelvis0 = self.modelvis.copy(deep=True)
        solve_jones(self.vis, modelvis0, self.modeljones, accum_opt = 0)

        modelvis1 = self.modelvis.copy(deep=True)
        solve_jones(self.vis, modelvis1, self.modeljones, accum_opt = 1)

        self.assertTrue( np.max(np.abs(modelvis0['vis'].data-self.vis['vis'].data)) < 1e-2 )
        self.assertTrue( np.std(modelvis0['vis'].data-self.vis['vis'].data) < 1e-2 )

        self.assertTrue( np.max(np.abs(modelvis1['vis'].data-self.vis['vis'].data)) < 1e-2 )
        self.assertTrue( np.std(modelvis1['vis'].data-self.vis['vis'].data) < 1e-2 )

        self.assertTrue( np.max(np.abs(modelvis1['vis'].data-modelvis0['vis'].data)) < 1e-9 )
        self.assertTrue( np.std(modelvis1['vis'].data-modelvis0['vis'].data) < 1e-9 )

        log.info("vis max:  {}".format(np.max(np.abs(self.vis['vis'].data))))
        log.info("diff max: {}".format(np.max(np.abs(modelvis0['vis'].data-self.vis['vis'].data))))
        log.info("diff max: {}".format(np.max(np.abs(modelvis1['vis'].data-self.vis['vis'].data))))
        log.info("diff max: {}".format(np.max(np.abs(modelvis1['vis'].data-modelvis0['vis'].data))))

        log.info("vis stdev:  {}".format(np.std(self.vis['vis'].data)))
        log.info("diff stdev: {}".format(np.std(modelvis0['vis'].data-self.vis['vis'].data)))
        log.info("diff stdev: {}".format(np.std(modelvis1['vis'].data-self.vis['vis'].data)))
        log.info("diff stdev: {}".format(np.std(modelvis1['vis'].data-modelvis0['vis'].data)))

    def test_algorithm(self):
        self.visSetup()

        modelvis1 = self.modelvis.copy(deep=True)
        solve_jones(self.vis, modelvis1, self.modeljones, testvis=self.vis, algorithm = 1)

        modelvis2 = self.modelvis.copy(deep=True)
        solve_jones(self.vis, modelvis2, self.modeljones, testvis=self.vis, algorithm = 2)

        self.assertTrue( np.max(np.abs(modelvis1['vis'].data-self.vis['vis'].data)) < 1e-2 )
        self.assertTrue( np.std(modelvis1['vis'].data-self.vis['vis'].data) < 1e-2 )

        self.assertTrue( np.max(np.abs(modelvis2['vis'].data-self.vis['vis'].data)) < 1e-2 )
        self.assertTrue( np.std(modelvis2['vis'].data-self.vis['vis'].data) < 1e-2 )

        log.info( np.max(np.abs(modelvis2['vis'].data-modelvis1['vis'].data)) )
        log.info( np.std(modelvis2['vis'].data-modelvis1['vis'].data) )
        self.assertTrue( np.max(np.abs(modelvis2['vis'].data-modelvis1['vis'].data)) < 1e-2 )
        self.assertTrue( np.std(modelvis2['vis'].data-modelvis1['vis'].data) < 1e-2 )

        log.info("vis max:  {}".format(np.max(np.abs(self.vis['vis'].data))))
        log.info("diff max: {}".format(np.max(np.abs(modelvis1['vis'].data-self.vis['vis'].data))))
        log.info("diff max: {}".format(np.max(np.abs(modelvis2['vis'].data-self.vis['vis'].data))))
        log.info("diff max: {}".format(np.max(np.abs(modelvis2['vis'].data-modelvis1['vis'].data))))

        log.info("vis stdev:  {}".format(np.std(self.vis['vis'].data)))
        log.info("diff stdev: {}".format(np.std(modelvis1['vis'].data-self.vis['vis'].data)))
        log.info("diff stdev: {}".format(np.std(modelvis2['vis'].data-self.vis['vis'].data)))
        log.info("diff stdev: {}".format(np.std(modelvis2['vis'].data-modelvis1['vis'].data)))

    def test_noise(self):
        vnchan=2
        ntimes=1
        self.visSetup(vnchan=vnchan, ntimes=ntimes)

        self.testvis = self.vis.copy(deep=True)

        sigma = 0.1
        shape = self.vis['vis'].shape
        self.assertEqual( len(shape), 4 )
        self.vis['vis'].data += sigma * ( np.random.randn(shape[0],shape[1],shape[2],shape[3]) + \
                                          np.random.randn(shape[0],shape[1],shape[2],shape[3]) * 1j )
        self.assertTrue( sigma > 0 )
        self.vis['weight'].data = np.ones(shape) / (sigma * sigma)

        log.info("Solving calibration")
        solve_jones(self.vis, self.modelvis, self.modeljones, testvis=self.testvis)

        # back of the envelope estimate of the error RMS level. 
        # gi_est ~ sum_j((Vij+error)*Mij - Mij*Mij) / sum_j(Mij*Mij)
        # gi_error ~ sum_j(error*Mij) / sum_j(Mij*Mij)
        # g_sigma ~ sqrt( sigma**2 / sum_j(Mij*Mij) )
        # vij ~ (1 + gi_error)*(1 + gj_error)*Mij
        # vij_error ~ Mij*gj_error + Mij*gi_error
        # vij_sigma ~ sqrt(2 * mean(Mij**2) * g_sigma**2)
        # chisq ~ mean( (vij_error)**2 )
        Mij = self.modelvis.sel({"antenna1": 0})['vis'].data[0,1::,0,0]
        gerr = np.sqrt( sigma**2 / np.sum(np.abs(Mij)**2) / float(vnchan*ntimes) )
        verr = 2 * np.sqrt( np.mean(np.abs(Mij)**2) * gerr**2 )

        log.info("vis max:  {}".format(np.max(np.abs(self.modelvis['vis'].data))))
        log.info("diff max: {}".format(np.max(np.abs(self.modelvis['vis'].data-self.testvis['vis'].data))))
        log.info("vis stdev:  {}".format(np.std(self.modelvis['vis'].data)))
        log.info("diff stdev: {}".format(np.std(self.modelvis['vis'].data-self.testvis['vis'].data)))
        log.info("est. stdev: {}".format(verr))
        self.assertTrue( np.abs(np.std(self.modelvis['vis'].data-self.testvis['vis'].data)/verr-1) < 0.5 )

        log.info("vis max:  {}".format(np.max(np.abs(self.vis['vis'].data))))

    # other tests:
    #  - same but with different inputs
    #  - multiple runs with different random sky models
    #  - runs that raise exceptions (e.g. wrong number of vis pols)

if __name__ == "__main__":
    unittest.main()

