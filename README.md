
Jones Solvers Package
=====================

<!-- [![Documentation Status](https://readthedocs.org/projects/jones-solvers/badge/?version=latest)](https://gitlab.com/ska-telescope/sdp/ska-sdp-jones-solvers?badge=latest) -->

A reference library of python-based antenna Jones matrix solvers for radio
interferometric calibration.

<!-- See the [package documentation](https://jones-solvers.readthedocs.io/en/latest/) -->
See the [package documentation](https://developer.skao.int/projects/ska-sdp-jones-solvers/en/latest/) 
for usage and performance information.

Requirements
------------

This package needs to have `Python 3` and `pip` installed, as well as the Radio
Astronomy Simulation, Calibration and Imaging Library (RASCIL) and its
dependencies. RASCIL requires Python 3.8 or greater.

Install 
-------

This package can be installed using pip:

```
pip install --extra-index-url=https://artefact.skao.int/repository/pypi-internal/simple ska-sdp-jones-solvers
```

Or, once RASCIL has been [installed using
pip](https://ska-telescope.gitlab.io/external/rascil/RASCIL_install.html)
clone then install the package to access the various support scripts:

```
git clone https://gitlab.com/ska-telescope/sdp/ska-sdp-jones-solvers.git  
cd ska-sdp-jones-solvers  
pip install .
```

Testing
-------

Unit tests can be run to check the installation and dependencies:

```
python tests/processing_components/test_solve_jones.py
```

Example driver scripts are also available:

```
python examples/scripts/run_jones_solvers.py  
python examples/scripts/run_AA0.5.py
```

