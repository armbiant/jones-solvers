
import os
import sys
import time
import copy

t0 = time.time()

import numpy as np

from numpy import sin as sin
from numpy import cos as cos

import matplotlib.pyplot as plt

from astropy.coordinates import SkyCoord
from astropy.time import Time
import astropy.units as u
import astropy.constants as consts

from rascil.data_models import PolarisationFrame
from rascil.processing_components import create_named_configuration
from rascil.processing_components import create_blockvisibility
from rascil.processing_components.util.geometry import calculate_azel
from rascil.processing_components.util.coordinate_support import lmn_to_skycoord

from rascil.processing_components.calibration.operations import create_gaintable_from_blockvisibility

from jones_solvers.processing_components import solve_jones

# ------------------------- #
# init plotting and logging #

import logging

log = logging.getLogger()
#log.setLevel(logging.DEBUG)
log.setLevel(logging.INFO)
log.addHandler(logging.StreamHandler(sys.stdout))

mpl_logger = logging.getLogger("matplotlib")
mpl_logger.setLevel(logging.WARNING)

np.set_printoptions(linewidth=-1)

t_import = time.time() - t0

# ------------------------- #

log.info("Init and predicting blockvisibility")

t0 = time.time()

#lowcore = create_named_configuration('LOWBD2-CORE')
lowcore = create_named_configuration('LOWBD2-CORE',rmax=250)
lon = 116.76444824 * np.pi / 180. # how can I extract these from lowcore?
lat = -26.82472208 * np.pi / 180.

#print(lowcore["xyz"])

vntimes = 1
integration_time = 1.0
times = (np.pi / 43200.0) * np.arange(0,vntimes*integration_time, integration_time)

vnchan = 1
channel_bandwidth = 1.0e6
frequency = np.arange(100.0e6, 100.0e6+vnchan*channel_bandwidth, channel_bandwidth)
channel_bandwidth = np.array(vnchan*[channel_bandwidth])

phasecentre = SkyCoord(ra=+15.0 * u.deg, dec=-45.0 * u.deg, frame='icrs', equinox='J2000')

# create empty blockvis with intrumental polarisation (XX, XY, YX, YY)
xVis = create_blockvisibility(lowcore, times, frequency, channel_bandwidth=channel_bandwidth, phasecentre=phasecentre,
                              integration_time=integration_time, polarisation_frame=PolarisationFrame("linear"),
                              weight=1.0)

assert xVis['vis'].shape[0]   == vntimes, "Shape inconsistent with specified number of times"
assert xVis['vis'].shape[2]   == vnchan, "Shape inconsistent with specified number of channels"
assert xVis['vis'].shape[3]   == 4, "Shape inconsistent with specified number of polarisations"
assert xVis['vis'].shape[0:3] == xVis["uvw_lambda"].data.shape[0:3], "vis & uvw_lambda have inconsistent shapes"
assert all(xVis['polarisation'].data == ['XX', 'XY', 'YX', 'YY']), "Polarisations inconsistent with expectations"

nvis = xVis["baselines"].shape[0]

# ------------------------- #

log.info("Gen sky model and updating vis")

Nsrc = 10

dist_source_max = 2.5 * np.pi/180.0

# sky model: randomise sources across the field
theta = 2.*np.pi * np.random.rand(Nsrc) 
phi = dist_source_max * np.sqrt( np.random.rand(Nsrc) )
l = sin(theta) * sin(phi)
m = cos(theta) * sin(phi)
n = np.sqrt(1-l*l-m*m) - 1
jy = 10 * np.random.rand(Nsrc)

#Nsrc = 1
#az = np.array([30 * np.pi / 180.])
#za = np.array([ 3 * np.pi / 180.])
#l = np.sin(az) * np.sin(za)
#m = np.cos(az) * np.sin(za)
#n = np.sqrt(1-l*l-m*m) - 1
#jy = np.array([1.0])

for src in range(0,Nsrc):

    # analytic response of short dipoles aligned NS & EW to sky xy polarisations
    # with an approx Gaussian taper for a 35m station
    srcdir = lmn_to_skycoord(np.array([l[src],m[src],n[src]]), phasecentre)
    ra  = srcdir.ra.value * np.pi / 180.
    dec = srcdir.dec.value * np.pi / 180.
    sep = srcdir.separation(phasecentre).value * np.pi / 180.
    diam = 35.;

    # need to set ha,dec, but need to be in time,freq loop
    for t in range(0,len(xVis['datetime'])):

        utc_time = xVis['datetime'].data[t]
        #azel = calculate_azel(location, utc_time, srcdir);            
        lst = Time(utc_time, location=(lon * u.rad, lat * u.rad)).sidereal_time('mean').value * np.pi / 12.
        ha = lst - ra

        J00 =  cos(lat)*cos(dec) + sin(lat)*sin(dec)*cos(ha)
        J01 = -sin(lat)*sin(ha)
        J10 =  sin(dec)*sin(ha)
        J11 =  cos(ha)
        J = np.array([[J00,J01],[J10,J11]], "complex")
        # components are unpolarised, so can form power product now
        JJ = J * J.conj().T

        for f in range(0,len(xVis['frequency'])):

            wl = consts.c.value / xVis['frequency'].data[f];
            sigma = wl/diam / 2.355;
            gain = np.exp( -sep*sep/(2*sigma*sigma) );

            srcbeam = JJ * gain

            # vis (time, baselines, frequency, polarisation) complex128

            uvw = xVis['uvw_lambda'].data[t,:,f,:]
            phaser = 0.5*jy[src] * np.exp( 2j*np.pi * (uvw[:,0]*l[src] + uvw[:,1]*m[src] + uvw[:,2]*n[src]) )

            assert all(xVis['polarisation'].data == ['XX', 'XY', 'YX', 'YY']), xVis['polarisation'].data

            xVis['vis'].data[t,:,f,0] += phaser * srcbeam[0,0]
            xVis['vis'].data[t,:,f,1] += phaser * srcbeam[0,1]
            xVis['vis'].data[t,:,f,2] += phaser * srcbeam[1,0]
            xVis['vis'].data[t,:,f,3] += phaser * srcbeam[1,1]

t_blockvis = time.time() - t0

# ------------------------- #

# Create a gain table with modest amplitude and phase errors

log.info("Applying calibration factors and noise")

t0 = time.time()

Jsigma = 0.1

# Some RASCIL functions to look into using
# gt_true = simulate_gaintable(xVis, phase_error=1.0, amplitude_error=0.1, leakage=0.1)
# gt_fit  = simulate_gaintable(xVis, phase_error=0.0, amplitude_error=0.0, leakage=0.0)

# generate a gaintable with a single timeslice (is in sec, so should be > 43200 for a 12 hr observation)
# could alternatively just use the first time step in the call
# "ValueError: Unknown Jones type P"
gt_true = create_gaintable_from_blockvisibility(xVis, timeslice=1e6, jones_type="G")
gt_fit  = create_gaintable_from_blockvisibility(xVis, timeslice=1e6, jones_type="G")

# set up references to the data
Jt = gt_true["gain"].data
Jm = gt_fit["gain"].data

stations = lowcore["stations"]
nstations = stations.shape[0]

for stn in range(0,nstations):

    # DAM not sure if this is fine, or if everything should be mapped to names in lowcore["stations"]

    # generate the starting model station gain error matrices. The same for all tests
    Jm[0,stn,0,:,:] = np.eye(2, dtype=complex)

    # generate the true station gain error matrices. Set to model matrices plus some Gaussian offsets
    Jt[0,stn,0,:,:] = Jm[0,stn,0,:,:] + Jsigma * ( np.random.randn(2,2) + 1j*np.random.randn(2,2) )

# Loop over visibilities and multiply in the Jones matrices
#  - assuming that unknown calibration errors are constant over time and frequency samples (i.e. is a snapshot)

# ------------------------- #

# Make copies of the vis and apply calibration factors

modelVis     = xVis.copy(deep=True)
noiselessVis = xVis.copy(deep=True)

# set up references to the data
stn1 = modelVis["antenna1"].data
stn2 = modelVis["antenna2"].data

for t in range(0,len(xVis['datetime'])):
    for f in range(0,len(xVis['frequency'])):

        # set up references to the data
        modelTmp     = modelVis['vis'].data[t,:,f,:]
        noiselessTmp = noiselessVis['vis'].data[t,:,f,:]

        for k in range(0,nvis):

            vis_in = np.reshape(modelTmp[k,:],(2,2))
            vis_out = Jm[0,stn1[k],0] @ vis_in @ Jm[0,stn2[k],0].conj().T
            modelTmp[k,:] = np.reshape(np.array(vis_out),(4))

            vis_in = np.reshape(noiselessTmp[k,:],(2,2))
            vis_out = Jt[0,stn1[k],0] @ vis_in @ Jt[0,stn2[k],0].conj().T
            noiselessTmp[k,:] = np.reshape(np.array(vis_out),(4))

# ------------------------- #

# Add noise to a visibility

sigma = 0.001

# Some RASCIL functions to look into using
# calculate_noise_blockvisibility(bandwidth, ...)
# addnoise_visibility(vis[, t_sys, eta, seed])

observedVis = noiselessVis.copy(deep=True)

shape = observedVis['vis'].shape
assert len(shape) == 4, "require 4 dimensions for blockvisibilty"

observedVis['vis'].data += sigma * ( np.random.randn(shape[0],shape[1],shape[2],shape[3]) + 
                                     np.random.randn(shape[0],shape[1],shape[2],shape[3]) * 1j )

if sigma > 0: observedVis['weight'].data = np.ones(shape) / (sigma * sigma)

t_updatevis = time.time() - t0

# ------------------------- #

# Solve for the gain factors

log.info("Solving calibration")

# Some RASCIL functions to look into using
# gtsol=solve_gaintable(cIVis, IVis, phase_only=False, jones_type="B")

log.info("Running algorithm 1 with defaults")

gt1 = gt_fit.copy(deep=True)
modelVis1 = modelVis.copy(deep=True)
t0 = time.time()
chisq1 = solve_jones(observedVis, modelVis1, gt1, testvis=noiselessVis, algorithm=1)
t_solving1 = time.time() - t0

log.info("Running algorithm 1 with nu=0.5")

gt1a = gt_fit.copy(deep=True)
modelVis1a = modelVis.copy(deep=True)
chisq1a = solve_jones(observedVis, modelVis1a, gt1a, testvis=noiselessVis, algorithm=1, nu=0.8)

log.info("Running algorithm 2 with defaults")

gt2 = gt_fit.copy(deep=True)
modelVis2 = modelVis.copy(deep=True)
t0 = time.time()
chisq2 = solve_jones(observedVis, modelVis2, gt2, testvis=noiselessVis, algorithm=2)
t_solving2 = time.time() - t0

# ------------------------- #

# print and plot some results

# set up references to the data (or do these copy? -- doesn't matter)
Jt  = gt_true["gain"].data[0,:,0,:,:]
J1  = gt1["gain"].data[0,:,0,:,:]
J1a = gt1a["gain"].data[0,:,0,:,:]
J2  = gt2["gain"].data[0,:,0,:,:]

fstr = "({0.real:+7.4f}{0.imag:+7.4f}i)"

log.info("")
log.info("       "+
         "                                  station 0                                     "+
         "                                  station 1                                     "+
         "                                  station 2                                     ")
log.info("Jfit   [["+fstr.format(J1[0,0,0])+","+fstr.format(J1[0,0,1])+
         ","+fstr.format(J1[0,1,0])+","+fstr.format(J1[0,1,1])+"]].   "+
         "[["+fstr.format(J1[1,0,0])+","+fstr.format(J1[1,0,1])+
         ","+fstr.format(J1[1,1,0])+","+fstr.format(J1[1,1,1])+"]].   "+
         "[["+fstr.format(J1[2,0,0])+","+fstr.format(J1[2,0,1])+
         ","+fstr.format(J1[2,1,0])+","+fstr.format(J1[2,1,1])+"]].   ")
log.info("Jtrue  [["+fstr.format(Jt[0,0,0])+","+fstr.format(Jt[0,0,1])+
         ","+fstr.format(Jt[0,1,0])+","+fstr.format(Jt[0,1,1])+"]]^-1 "+
         "[["+fstr.format(Jt[1,0,0])+","+fstr.format(Jt[1,0,1])+
         ","+fstr.format(Jt[1,1,0])+","+fstr.format(Jt[1,1,1])+"]]^-1 "+
         "[["+fstr.format(Jt[2,0,0])+","+fstr.format(Jt[2,0,1])+
         ","+fstr.format(Jt[2,1,0])+","+fstr.format(Jt[2,1,1])+"]]^-1 ")
U0 = J1[0] @ np.linalg.inv(Jt[0])
U1 = J1[1] @ np.linalg.inv(Jt[1])
U2 = J1[2] @ np.linalg.inv(Jt[2])
log.info("U      [["+fstr.format(U0[0,0])+","+fstr.format(U0[0,1])+
         ","+fstr.format(U0[1,0])+","+fstr.format(U0[1,1])+"]]    "+
         "[["+fstr.format(U1[0,0])+","+fstr.format(U1[0,1])+
         ","+fstr.format(U1[1,0])+","+fstr.format(U1[1,1])+"]]    "+
         "[["+fstr.format(U2[0,0])+","+fstr.format(U2[0,1])+
         ","+fstr.format(U2[1,0])+","+fstr.format(U2[1,1])+"]]    ")
pref = np.exp(-1j*np.angle(U0[0,0]))
U0 *= pref
U1 *= pref
U2 *= pref
log.info("UrefXX [["+fstr.format(U0[0,0])+","+fstr.format(U0[0,1])+
         ","+fstr.format(U0[1,0])+","+fstr.format(U0[1,1])+"]]    "+
         "[["+fstr.format(U1[0,0])+","+fstr.format(U1[0,1])+
         ","+fstr.format(U1[1,0])+","+fstr.format(U1[1,1])+"]]    "+
         "[["+fstr.format(U2[0,0])+","+fstr.format(U2[0,1])+
         ","+fstr.format(U2[1,0])+","+fstr.format(U2[1,1])+"]]    ")
log.info("solve time = {:0.1f} sec".format(t_solving1))
log.info("refXX = exp({:.6f}i)".format(np.angle(pref)))

log.info("")
log.info("       "+
         "                                  station 0                                     "+
         "                                  station 1                                     "+
         "                                  station 2                                     ")
log.info("Jfit   [["+fstr.format(J2[0,0,0])+","+fstr.format(J2[0,0,1])+
         ","+fstr.format(J2[0,1,0])+","+fstr.format(J2[0,1,1])+"]].   "+
         "[["+fstr.format(J2[1,0,0])+","+fstr.format(J2[1,0,1])+
         ","+fstr.format(J2[1,1,0])+","+fstr.format(J2[1,1,1])+"]].   "+
         "[["+fstr.format(J2[2,0,0])+","+fstr.format(J2[2,0,1])+
         ","+fstr.format(J2[2,1,0])+","+fstr.format(J2[2,1,1])+"]].   ")
log.info("Jtrue  [["+fstr.format(Jt[0,0,0])+","+fstr.format(Jt[0,0,1])+
         ","+fstr.format(Jt[0,1,0])+","+fstr.format(Jt[0,1,1])+"]]^-1 "+
         "[["+fstr.format(Jt[1,0,0])+","+fstr.format(Jt[1,0,1])+
         ","+fstr.format(Jt[1,1,0])+","+fstr.format(Jt[1,1,1])+"]]^-1 "+
         "[["+fstr.format(Jt[2,0,0])+","+fstr.format(Jt[2,0,1])+
         ","+fstr.format(Jt[2,1,0])+","+fstr.format(Jt[2,1,1])+"]]^-1 ")
U0 = J2[0] @ np.linalg.inv(Jt[0])
U1 = J2[1] @ np.linalg.inv(Jt[1])
U2 = J2[2] @ np.linalg.inv(Jt[2])
log.info("U      [["+fstr.format(U0[0,0])+","+fstr.format(U0[0,1])+
         ","+fstr.format(U0[1,0])+","+fstr.format(U0[1,1])+"]]    "+
         "[["+fstr.format(U1[0,0])+","+fstr.format(U1[0,1])+
         ","+fstr.format(U1[1,0])+","+fstr.format(U1[1,1])+"]]    "+
         "[["+fstr.format(U2[0,0])+","+fstr.format(U2[0,1])+
         ","+fstr.format(U2[1,0])+","+fstr.format(U2[1,1])+"]]    ")
pref = np.exp(-1j*np.angle(U0[0,0]))
U0 *= pref
U1 *= pref
U2 *= pref
log.info("UrefXX [["+fstr.format(U0[0,0])+","+fstr.format(U0[0,1])+
         ","+fstr.format(U0[1,0])+","+fstr.format(U0[1,1])+"]]    "+
         "[["+fstr.format(U1[0,0])+","+fstr.format(U1[0,1])+
         ","+fstr.format(U1[1,0])+","+fstr.format(U1[1,1])+"]]    "+
         "[["+fstr.format(U2[0,0])+","+fstr.format(U2[0,1])+
         ","+fstr.format(U2[1,0])+","+fstr.format(U2[1,1])+"]]    ")
log.info("solve time = {:0.1f} sec".format(t_solving2))
log.info("refXX = exp({:.6f}i)".format(np.angle(pref)))

# ------------------------- #

fstr = ' - {:<25} {:6.1f} sec'
log.info("")
log.info("Timing:")
log.info(fstr.format("package imports", t_import))
log.info(fstr.format("init and predict blockvis", t_blockvis))
log.info(fstr.format("apply corruptions", t_updatevis))
tstr = fstr.format("solver 1", t_solving1)
if len(chisq1) > 0: tstr+=" for {} iterations".format(len(chisq1))
log.info(tstr)
tstr = fstr.format("solver 2", t_solving2)
if len(chisq2) > 0: tstr+=" for {} iterations".format(len(chisq2))
log.info(tstr)
log.info("")

# ------------------------- #

plt.figure(num=0, figsize=(14,8), facecolor='w', edgecolor='k')

# back of the envelope estimate of the error RMS level. 
# gi_est ~ sum_j((Vij+error)*Mij - Mij*Mij) / sum_j(Mij*Mij)
# gi_error ~ sum_j(error*Mij) / sum_j(Mij*Mij)
# g_sigma ~ sqrt( sigma**2 / sum_j(Mij*Mij) )
# vij ~ (1 + gi_error)*(1 + gj_error)*Mij
# vij_error ~ Mij*gj_error + Mij*gi_error
# vij_sigma ~ sqrt(2 * mean(Mij**2)) * g_sigma
# chisq ~ mean( (vij_error)**2 )
Mij = modelVis.sel({"antenna1": 0})['vis'].data[0,1::,0,0]
# could use the mean of all vis and multiply by nstn-1. Careful of autos though
g_sigma = np.sqrt( sigma**2 / np.sum(np.abs(Mij)**2) / float(vntimes*vnchan) )
v_sigma = 2 * np.sqrt( np.mean(np.abs(Mij)**2) * g_sigma**2 )

ax = plt.subplot(111)
ax.set_yscale('log')
plt.plot( chisq1, '.-', label="RTS" )
plt.plot( chisq1a, '.-', label="RTS with slow conv" )
plt.plot( chisq2, '.-', label="Yanda" )
# also it is the error, not the chisq, so square it. Also not exactly the right operation
if sigma>0: plt.plot( ax.get_xlim(), 2*v_sigma**2*np.ones(2), '--', label="Error floor estimate" )
plt.xlabel("iteration", fontsize=14)
plt.ylabel("chisq error", fontsize=14)
plt.legend(loc=1, fontsize=14, frameon=False)
plt.grid()

plt.show()

