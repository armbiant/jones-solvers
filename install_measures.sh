#!/bin/sh

if [ $# -lt 1 ]; then
    echo "Usage: $0 <casacore-measure-dir>" 1>&2
    exit 1
fi

target_dir="$1"

if [ -d "$target_dir" ]; then
    exit 0
fi

mkdir -p "$target_dir"
cd "$target_dir"

if ! command -v wget &> /dev/null; then
    apt-get -y update
    apt-get install -y wget
fi

wget -O measures.tar.gz ftp://ftp.astron.nl/outgoing/Measures/WSRT_Measures.ztar
tar xf measures.tar.gz
rm measures.tar.gz
