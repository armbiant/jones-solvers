# Use bash shell with pipefail option enabled so that the return status of a
# piped command is the value of the last (rightmost) commnand to exit with a
# non-zero status. This lets us pipe output into tee but still exit on test
# failures.
SHELL = /bin/bash
.SHELLFLAGS = -o pipefail -c
PYPI_REPOSITORY_URL=https://artefact.skao.int/repository/pypi-internal
PYTHON_LINT_TARGET = jones_solvers/
PYTHON_SWITCHES_FOR_FLAKE8 = --select E --ignore=E501,E203,E711,E712,F401,F403,W292,C0103
PYTHON_SWITCHES_FOR_PYLINT = --disable R0801,C0103,C0114,C0304,C0301,W1202,W0612,C0209,C0121,R1702,R0913,R1716,R1714,R0914,R0912,R0915,C0414,C0116
DOCS_SOURCEDIR= docs/
DOCS_SPHINXOPTS = -W --keep-going

include .make/base.mk
include .make/python.mk

install_package:
	pip install --extra-index-url=$(PYPI_REPOSITORY_URL)/simple -e .

python-pre-test: install_package
	pip install -r test-requirements.txt

python-pre-lint:
	pip install -r lint-requirements.txt

